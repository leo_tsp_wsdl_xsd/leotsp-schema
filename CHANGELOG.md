# 1.7 (Release geplant - Anwendung im Herbst 2025)

[Diff](https://gitlab.com/leo_tsp_wsdl_xsd/leotsp-schema/-/compare/1.6...1.7)

* TippFehler 'SmcbSperrAutraege' in in leo-tsp.wsdl korrigiert #15
* Optionaler Vorname im Sperrelement #23
* Anpassung der Druckzeilen auf eine Zeichenlänge von 80 Zeichen #26
* Entfernen veralteter Angaben, um den Stand "gemSpec_OID_V3.20.0" für Institutionstypen und Berufsgruppen zu erhalten #24
* Entfernen veralteter Ländercodes / Abgleich mit ISO 3166  #25

# 1.6-4 (2025-03-01)

* Erweiterung um neue Angaben nach "gemSpec_OID_V3.20.0" für Institutionstypen und Berufsgruppen #24
* Erweiterung der Ländercodes bzw. Anpassung an ISO 3166. Veraltete Codes wurden mit Kommentar versehen und werden zu Schemaversion 1.7 entfallen. #25

# 1.6-3 (2024-05-21)

* Eweiterung um neue Angaben nach "gemSpec_OID_V3.18.0" Kapitel "3.5.1.3 OID-Festlegung für Institutionstypen für die SMC-B" #20
* Eweiterung um neue Angaben nach "gemSpec_OID_V3.18.0" Kapitel "3.5.1.1 OID-Festlegung Rolle für Berufsgruppen" #20
* Datentyp Druckzeilen: Zeile 3 auf 80 Zeichen erweitern #21

# 1.6-2 (2024-03-06)

* Eweiterung um neue Angabe 'Ergotherapeut/-in' nach "gemSpec_OID_V3.12.2" Kapitel "3.5.1.1 OID-Festlegung Rolle für Berufsgruppen" #19

# 1.6-1 (2023-03-27)

* Workaround für die Validierungsprobleme bei der Pflichangabe "Kartengeneration" #17

# 1.6 (2023-03-27) 

[Diff](https://gitlab.com/leo_tsp_wsdl_xsd/leotsp-schema/-/compare/1.5...1.6)

* Erweiterung von InstProfessionItemKey (Institutionen) auf Basis von gemSpec_OID Version 3.12.2 #8
* Erweiterung von ProfessionItemKey (Berufsgruppen) auf Basis von gemSpec_OID Version 3.12.2 #8
* Geschlecht/Anrede um die Ausprägung 'keine Angabe' ergänzt #6
* AusweisArt entfernt #2
* Angabe der konkreten Zertifikats-Algorithmen aufnehmen (Ablösung der Angabe aktuelle/vorherige Version) #7
* Aufnahme des Elementes Kartengeneration im ProdResultType mit den aktuellen Werte G2.0 und G2.1 #7
* Aufnahme des optionalen Elementes Antragstyp im Vorbefüllungselement #13
* Korrektur des Eintrages "Gesundheits- und Krankenpfleger/-in, Gesundheits- und Kinderkrankenpfleger/-in" der Enumeration 'ProfessionItemKey' #14

Die Veröffentlichungen der Version 1.6 vom 28.09.2022 und 31.01.2023 sind aufgrund von #14 fehlerhaft. Bitte nur die Version vom 27.03.2023 nutzen!!!


# 1.5 (2020-05-14)

[Diff](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/compare/1.4...1.5) (alle Verlinkungen zeigen auf ein geschütztes Projekt)

 * Limit und Offset Parameter (Blättern) für alle Methoden bei denen mehrere Anträge zurück kommen [\#20](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/20)
 * Sortierung für für Exporte bei mehreren Anträgen [\#21](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/21)
 * Anrede überarbeitet - Geschlecht "divers" eingeführt [\#33](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/33)
 * Vorbefüllung mit „leerem“ Wert ermöglicht [\#34](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/34)
 * Default-Wert für neue Telematik-ID entfernt [\#35](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/35)
 * Überblicksanfrage - Umgang mit dem Pflichtfeld "SperrPDF" wurde in Dokumentation ergänzt [\#36](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/36)
 * AnzahlKarten auch für den HBA-Bereich ermöglicht [\#37](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/37)
 * AusweisArt / Ersatzkarte als deprecated markiert [\#40](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/40)
 * Implementierungs-relevante Dokumentation des Titels in ErklaerungType angepasst [\#39](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/39)
 * Aufnahme weiterer InstProfessionItemKey für die Ausgabe von SMC-B ORG [\#42](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/issues/42)
 * Dokumentationen angepasst. Zentrale [CHANGELOG.md](https://gitlab.com/leo_tsp_wsdl_xsd/wsdl-xsd/-/blob/1.5/CHANGELOG.md) im Wurzelverzeichnis erstellt

Versionen vor 1.5 sind nur noch im privaten Projekt zu finden und für aktuelle Entwicklungen nicht weiter relevant.
