# LEO-TSP-AG

Die LEO-TSP-AG ist eine Arbeitsgruppe mit dem Ziel einen regelmäßigen Austausch zwischen allen an der Herausgabe von HBAs und SMC-Bs Beteiligten zu fördern.

Weiter wird in der Arbeitsgruppe die technische Schnittstelle zwischen den Beteiligten gepflegt.

# Dieses Projekt

Dieses Projekt bildet die technische Grundlage der einheitlichen Schnittstelle zwischen den Kartenherausgebern und den Kartenproduzenten ab. Die Schnittstelle nutzt dabei ein XML-Schema als Grundlage mit Spezialisierungen für HBA und SMC-B.
Die SOAP-Schnittstelle wird schließlich in einer WSDL beschrieben.

# Schnittstelle

Die serverseitige Implementierung findet auf Seiten der Kartenproduzenten statt. Sie stellen den SOAP-Endpunkt zur Verfügung. Die Grundlage dazu bildet die WSDL.
Sie besteht im wesentlichen aus den folgenden Funktionen

## Export

Exporte/Suchen anhand unterschiedliche Kriterien ausführen. Zum Beispiel um den Fortschritt von Anträgen zu verfolgen.

## Vorbefüllung

Vorbefüllung von Daten zur Person (HBA) oder Institution inkl Person (SMC) die für den weiteren Antragsprozess verwenden werden sollen.

## Freigabe

Freigabe eines Antrags durch den zuständen Kartenherausgeber.

## Sperre

Ausführen einer Sperranforderung durch einen Kartenherausgeber.
